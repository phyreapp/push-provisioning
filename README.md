# Paynetics Push Provisioning SDK/service

For an overview of the service's operation see the sequence diagrams in file sequence diagram.png / sequence diagram.pdf.


Clients MUST implement an endpoint responsible for push provisioning request authorization:

* See file is-provisioning-authorized-endpoint.md for API contract.

* The payload of the request and response must be JSON.

* The headers `Content-Digest`, `Signature-Input`, and `Signature` conform to the Draft specification "HTTP Message Signatures" and allow you to verify the authenticity and integrity of the request's payload. Clients MUST verify the signature using the provided ed25519 public key (paynetics-push-provisioning-ed25519-signing-key.public.pem).

* The endpoint must be exposed only using HTTPS and we recommend TLS 1.3 (https://wiki.mozilla.org/Security/Server_Side_TLS#Modern_compatibility) but can fall back to TLS 1.2 (https://wiki.mozilla.org/Security
/Server_Side_TLS#Intermediate_compatibility_.28recommended.29) with a highly limited cipher suite whitelist.
