#### POST https://<client-domain>/.../v1/is-provisioning-authorized

##### Request
###### Headers
```
// all headers below are required and values shown are just examples
X-Request-Id: ... // UUIDv4, different for each request, acts as a tracing id
Content-Type: application/json
...
Content-Digest: sha-512=:WZDPaVn/7XgHaAy8pmojAkGWoRx2UFChF41A2svX+TaPm+AbwAgBWnrIiYllu7BNNyealdVLvRwEmTHWXvJwew==:
Signature-Input: sig1=("content-digest");created=1647845712;keyid="phyre-ed25519-sign-key"
Signature: sig1=:LAH8BjcfcOcLojiuOBFWn0P5keD3xAOuJRGziCLuD8r5MW9S0RoXXLzLSRfGY/3SF8kVIkHjE13SEFdTo4Af/fJ/Pu9wheqoLVdwXyY/UkBIS1M8Br==:
```

###### Body
```json5
{
  "clientPaymentCardId": "...", // required, non-empty string
  "clientAuthenticationId": "..." // required, non-empty string
}
```

##### Response
**200 OK**

When provisioning is authorized:
```json5
X-Request-Id: <same from request header>
Content-Type: application/json
{
  "authorized": true, // required, boolean
  "publicPaymentCardToken": "957182741" // required, non-empty string
}
```

When provisioning is not authorized:
```json5
X-Request-Id: <same from request header>
Content-Type: application/json
{
  "authorized": false // required, boolean
}
```
